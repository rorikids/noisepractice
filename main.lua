local suit = require "suit"
local vopef = require "waffle"

local scale = {text = "150.0"}
local octaves = {text = "8"}
local sliderLagunaridad = {value = 2, min = 1, max = 5}
local persistencia = {text = "0.5"}
local semilla = {text = "1"}

function  love.load()
  noiseWidth = 512
  noiseHeigth = 512
  imageGenerated = false
  love.graphics.setBackgroundColor(30, 30, 30)
end

function love.update(dt)
    suit.layout:reset(1200,50, 20, 20)
  if love.keyboard.isDown('escape') then
    love.event.push('quit')
  end
	if suit.Button("Generar", {""}, suit.layout:row(200,40)).hit then
    imageGenerated = true
    mapa = generateImage()
  end
  love.graphics.setColor(255, 255, 255, 255)

  suit.layout:push(suit.layout:row())
  suit.Label("Escala:",{align='left'}, suit.layout:row(100, 20))
  --Scale
  if suit.Input(scale, suit.layout:col(150,30)).submitted then
    print(scale.text)
  end
	suit.layout:pop()

  suit.layout:push(suit.layout:row())
  suit.Label("Octavios:",{align='left'}, suit.layout:row(100, 20))

  if suit.Input(octaves, suit.layout:col(150,30)).submitted then
    print(octaves.text)
  end
	suit.layout:pop()


  suit.layout:push(suit.layout:row())
  suit.Label("Lagunaridad:",{align='left'}, suit.layout:row(100, 20))
  suit.Slider(sliderLagunaridad, suit.layout:col(200, 20))
  suit.Label(("%.02f"):format(sliderLagunaridad.value), suit.layout:col(40))
	suit.layout:pop()

  suit.layout:push(suit.layout:row())
  suit.Label("Persistencia:",{align='left'}, suit.layout:row(100, 40))
  if suit.Input(persistencia, suit.layout:col(150,30)).submitted then
    print(persistencia.text)
  end
  suit.layout:pop()

  suit.layout:push(suit.layout:row())
  suit.Label("Semilla:",{align='left'}, suit.layout:row(100, 40))
  if suit.Input(semilla, suit.layout:col(150,30)).submitted then
    print(semilla.text)
  end
  suit.layout:pop()

end

function love.draw()
  if imageGenerated then
    love.graphics.setColor(0, 0, 0)
    --marco
    --love.graphics.rectangle("fill", 48, 48, 516, 516)
    love.graphics.setColor(255, 255, 255)
    mapa.water:setFilter("nearest", "nearest")
    mapa.terrain:setFilter("nearest", "nearest")

    love.graphics.draw(mapa.terrain, 50, 0, 0, 2, 2)

    love.graphics.setColor(255, 0, 0)

    for i=0,mapa.citiesSize do
      love.graphics.rectangle("fill",50+mapa.cities[i].x*2, mapa.cities[i].y*2, 2, 2)
    end

    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(mapa.water, 50, 0, 0, 2, 2)

  end
  suit.draw()
end

function generateImage()

  scaleValue = tonumber(scale.text)
  if scaleValue < 0.0 then
    scaleValue = 0.001
  end

  octavesNumber = tonumber(octaves.text)
  if octavesNumber < 1 then
    octavesNumber = 1
  end

  persistenciaValue = tonumber(persistencia.text)
  if persistenciaValue < 0.0 then
    persistenciaValue = 0.001
  end

  lagunaridadValue = tonumber(sliderLagunaridad.value)
  if lagunaridadValue < 0.0 then
    lagunaridadValue = 0.001
  end

  semillaValue = tonumber(semilla.text)
  if semillaValue < 0.0 then
    semillaValue = 1
  end
  return waffle.generateImage(512, 512, scaleValue, octavesNumber, persistenciaValue, lagunaridadValue, semillaValue)
end

function love.textinput(t)
	-- forward text input to SUIT
	suit.textinput(t)
end

function love.keypressed(key)
	love.keyboard.setKeyRepeat( true )
	suit.keypressed(key)
end
