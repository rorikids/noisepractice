voronoi = require "voronoi"

waffle = {}

--call this method, it's return an entire image
--this will be cahnges in future t return a table with additional map data
function waffle.generateImage(height, width, scaleValue, octavesNumber, persistenciaValue, lagunaridadValue, semillaValue)
  mapa = {}
  ruido = generateNoise(height, width, scaleValue, octavesNumber, persistenciaValue, lagunaridadValue, semillaValue)
  normalizeNoise(minNoise, maxNoise, ruido)
  mapa.cities = defineCities(semillaValue, ruido)
  noiseImageData = love.image.newImageData(noiseWidth, noiseHeigth)
  setImageData(noiseWidth,noiseHeigth)
  mapa.water = love.graphics.newImage(noiseImageData)
  noiseImageData = love.image.newImageData(noiseWidth, noiseHeigth)
  setTerrain(noiseWidth,noiseHeigth)
  mapa.terrain = love.graphics.newImage(noiseImageData)
  return mapa
end

--main algorithm of the library, generates the perlin noise itself
function generateNoise(mapWidth, mapHeigth, noiseScale, octaves, persistencia, lagunaridad, seed)

  love.math.setRandomSeed(seed)
  pseudoRandomSeedX = {}
  pseudoRandomSeedY = {}
  for i=1,octaves+1 do
      pseudoRandomSeedX[i] = love.math.random(-100000, 100000)
      pseudoRandomSeedY[i] = love.math.random(-100000, 100000)
  end

  halfWidth = mapWidth / 2
  halfHeight = mapHeigth / 2

  noise = {}

  minNoise = 10000.0
  maxNoise = -10000.0

  for i=0,mapWidth-1 do
    noise[i]= {}
    for j=0,mapHeigth-1 do

      amplitud = 1.0
      frecuencia = 1.0
      alturaRuido = 0

      for k=0,octaves do
        muestraX = (i-halfWidth)/noiseScale * frecuencia + pseudoRandomSeedX[octaves]
        muestraY = (j-halfHeight)/noiseScale * frecuencia + pseudoRandomSeedY[octaves]
        noiseValue = love.math.noise(muestraX, muestraY) * 2 -1


        amplitud = amplitud * persistencia
        frecuencia = frecuencia * lagunaridad
        alturaRuido =  alturaRuido + noiseValue * amplitud
      end

      if minNoise > alturaRuido then
        minNoise = alturaRuido
      end
      if maxNoise < alturaRuido then
        maxNoise = alturaRuido
      end

      noise[i][j] = alturaRuido

    end
  end
  return noise
end

--This must be modifies to create a new image for Voronoi
--instead of modify noiseData
function defineCities(seed, ruido)
  count= 0
  cities = {}
  love.math.setRandomSeed(seed)
  multiplicadorI = 64
  multiplicadorJ = 64
  desplazamientoX = 0
  desplazamientoY = 0
  fijado = false

  for i=0, 7 do
    for j=0, 7 do

      if not (ruido[32+i*multiplicadorI][32+j*multiplicadorJ] < 0.58) then
        fijado = false
        while not fijado do
          desplazamientoX = love.math.random(-15, 15)
          desplazamientoY = love.math.random(-15, 15)
          if not (ruido[32+i*multiplicadorI+desplazamientoX][32+j*multiplicadorJ+desplazamientoY] < 0.58) then
            fijado = true
          end
        end
        cities[count] = {x = 32+i*multiplicadorI+desplazamientoX, y = 32+j*multiplicadorJ+desplazamientoY}
        count = count + 1
      end
    end
  end
  mapa.citiesSize = count-1
  return cities
end

--here is where all the magic happens, the colours are assigned to the image
function setImageData(w, h)
  for i=0,w-1 do
    for j=0,h-1 do

      --oceano
      if (noise[i][j] > 0.45) and (noise[i][j] < 0.58) then
        noiseImageData:setPixel(i, j, 35, 80, 142, 255)
      else
        if noise[i][j] < 0.45 then
          noiseImageData:setPixel(i, j, 63, 63, 116, 255)
        else
          noiseImageData:setPixel(i, j, 0, 0, 0, 0)
        end
      end
    end
  end
end

function setTerrain(w, h)
  for i=0,w-1 do
    for j=0,h-1 do

      if noise[i][j] < 0.58 then
        noiseImageData:setPixel(i, j, 0, 0, 0, 0)
      end
      --arena
      if noise[i][j] > 0.58 then
        noiseImageData:setPixel(i, j, 255, 226, 91, 255)
      end
      --bosque
      if noise[i][j] > 0.60 then
        noiseImageData:setPixel(i, j, 106, 190, 48, 255)
      end
      --bosque oscuro
      if noise[i][j] > 0.70 then
        noiseImageData:setPixel(i, j, 82, 146, 37, 255)
      end
      --montañas
      if noise[i][j] > 0.87 then
        noiseImageData:setPixel(i, j, 96, 83, 66, 255)
      end
      --montañas altas
      if noise[i][j] > 0.95 then
        noiseImageData:setPixel(i, j, 240, 240, 240, 255)
      end
    end
  end

end

--en casos extremos que no haya valores cercano al 0 o al 1, normaliza la matriz,
--dejandola llena de valores entre 0 y 1
function normalizeNoise(minValue, maxValue, matrix)
  for i=0, table.getn(matrix)-1 do
    for j=0,table.getn(matrix[i])-1 do
      matrix[i][j] = (matrix[i][j] - minValue) / (maxValue - minValue)
    end
  end
  --no necesita devolver la matriz ya que opera directamente sobre esta
end
