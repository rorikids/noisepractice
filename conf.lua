function love.conf(t)
  t.window.title = "Perlin Noise Generator"         -- The window title (string)
  t.window.icon = nil       --"images/DataReignEye.png"                 -- Filepath to an image to use as the window's icon (string)
  t.window.width = 1800               -- The window width (number)
  t.window.height = 1048               -- The window height (number)
  t.window.borderless = false         -- Remove all border visuals from the window (boolean)
  t.window.resizable = true          -- Let the window be user-resizable (boolean)
end
